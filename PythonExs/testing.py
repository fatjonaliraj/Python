funx = ('x+y', 'x*y', 'x-y', 'x/y')

def createfunctions():
    def mbledhje(a, b):
        return a+b
    def zbritje(a, b):
        return a-b
    shumzim = lambda a, b : a * b
    pjestim = lambda a, b : float(a / b)
    return (mbledhje, zbritje, shumzim, pjestim)

myfunc = [createfunctions()]
a = 5
b = 10

for mbledhje, zbritje, shumzim, pjestim in myfunc:
    print str(mbledhje(a,b))
    print str(zbritje(a,b))
    print str(shumzim(a,b))
    print str(pjestim(a,b))
    
