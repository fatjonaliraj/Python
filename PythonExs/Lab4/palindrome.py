import string

class palindrome():
    def __init__(self, myString):
        
        self.myString       = self.cleanmystring(myString)
        self.reversedStr    = self.myString[::-1]
        
    def testPalindrome(self):
        
        if self.myString == self.reversedStr:
            return "Is a palindrome"
        else:
            return "Is not a palindrome"
    
    def cleanmystring(self, a):
        
        return (a.upper()).translate(string.maketrans("", ""), string.punctuation).replace(" ", "")
    
    def __str__(self):
        return self.myString + " " + str(self.testPalindrome())

if __name__ == '__main__':
    print palindrome(input("Enter your text: "))
