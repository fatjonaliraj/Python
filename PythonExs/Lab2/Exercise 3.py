import math

def sin(x, n):
    
    mySum = 0
    
    for i in range(0, n):
        mySum += pow(-1, i) * pow(x, (2*i+1))/math.factorial(2*i + 1)

    print float(mySum)
    
sin(30, 3)
