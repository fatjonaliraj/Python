#readTxt(input("Insert your file path: "))
#readTxt("C:\Users\GERMAN\Desktop\mytxt.txt")

def readTxt(pathFile):
    import re
    
    myText      = open(pathFile, "r")
    lines       = myText.readlines()
    wordsArray  = []
    word        = ""
    j           = 0
    
    for i in lines:
        word    = i.split()
        j       = 0
        
        while j < len(word)-1:
            word[j] = re.sub(r'[^a-zA-Z0-9 ]',r'', word[j])
            
            if not word[j]:
                del (word[j])
                j = j - 1
                
            j += 1

        if str(word) != '[]':
            wordsArray += word

    return wordsArray

def freqs(pathFile, number):
    from collections import OrderedDict
    counter     = 0
    myMap       = {}
    sortedMap   = {}
    myarray     = readTxt(pathFile)
    
    for i in myarray:
        counter = 0
        for j in myarray:
            if i == j :
                counter += 1
        myMap.update({i: counter})

    sortedMap = OrderedDict(sorted(myMap.items(), key=lambda t: t[1]))

    for k in sortedMap:
        if sortedMap.values() > 5:
            print k
