def table(number, scale):
    
    scales  = ["celcius", "fahrenheit", "kelvin", "rankine", "delisle"]
    myTable = "This is a temperature scales table \n"
    
    for i in scales:
        if i.upper() == scale.upper():
            continue
        if i.upper() == "CELCIUS":
            myTable = myTable + str(number) + "\t celcius \n"
        elif i.upper() == "FAHRENHEIT":
            myTable = myTable + str(eval(str(number + 32))) + "\t fahrenheit \n"
        elif i.upper() == "KELVIN":
            myTable = myTable + str(eval(str(number + 273.15))) + "\t kelvin \n"
        elif i.upper() == "RANKINE":
            myTable = myTable + str(eval(str(number + 491.67))) + "\t rankine \n"
        elif i.upper() == "DELISLE":
            myTable = myTable + str(eval(str(number*1.5 - 100))) + "\t delisle \n"

    print (myTable)

number = input("Enter your number: ")
scale = input("Enter your scale: ")
table(number, scale)
