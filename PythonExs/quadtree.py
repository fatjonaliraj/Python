import math

class quadtree:
    def __init__(self, array):
        self.node = []
        self.step = int(math.sqrt(len(array)))
        self.tree = self.maketree(array)
    def maketree(self, array):
        return [array[i : (i+ self.step)] for i in range(0, len(array), self.step)]
    def __str__(self):
        return str(self.tree)

print(quadtree([0, 0, 1, 0, 1, 1, 0, 0, 1, 0, 1, 0, 0, 1, 0, 0]))
