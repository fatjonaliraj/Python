import calendar
from datetime import datetime

currentYear = datetime.now().year
numOfLeap   = 0
day         = 29
month       = 7
year        = 2016

for i in range(currentYear, 3000):
    if calendar.isleap(i):
        print "The next leap year is: " + str(i)
        break

for i in range(2000, 2050):
    if calendar.isleap(i):
        numOfLeap += 1

if numOfLeap > 0:
    print "Betwen year 2000 and 2050 will be " + str(numOfLeap) + " leap years"

weekDay         = calendar.weekday(year, month, day)
arrayDayName    = calendar.day_name

print "Date 29 July, 2016 must be " + arrayDayName[weekDay]
