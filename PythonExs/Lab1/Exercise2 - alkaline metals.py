alkaline_earth_metals           = {56:"barium", 4:"beryllium", 20:"calcium", 12:"magnesium", 88:"radium", 38:"strontium"}
sorted_alkaline_earth_metals    = sorted(alkaline_earth_metals)

for i in sorted_alkaline_earth_metals:
    print str(i) + ":\t" + alkaline_earth_metals[i]
    
print alkaline_earth_metals[(sorted_alkaline_earth_metals[len(sorted_alkaline_earth_metals) - 1])] + " is the highest element in the array"
