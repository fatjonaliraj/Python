import re

def buildFunc(pattern, search, replace):
    def match_rules(word):
        return re.search(pattern, word)
    apply_rule = lambda word : re.sub(search, replace, word)
    return (match_rules, apply_rule)

patterns = (('[sxz]$', '$', 'es'), ('[^aeioudgkprt]h$', '$', 'es'),
            ('(qu|[^aeiou]y$)', 'y$', 'ies'), ('$', '$', 's'))

rules = [buildFunc(pattern, search, replace) for (pattern, search, replace) in patterns]

if __name__ == "__main__":
    thisinput = input("Enter your word: ")
    for i in rules:
        if i[0](thisinput):
            print i[1](thisinput)
            break
